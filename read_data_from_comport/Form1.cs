﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Forms;
using System.IO;
using System.IO.Ports;
using System.Diagnostics;

namespace read_data_from_comport
{
    public partial class Form1 : Form
    {
        BinaryWriter writer = null; // поток на запись в файл
        FileStream f_use = null;  // работа с файлом
        List<byte> buffreader = new List<byte>(); // задает массив
        byte[] mdata = null; // буфер принятых данных
        int ecgSizeFromSP = 0; // размер принимаемой кардио
        string ecgFileName = ""; // имя переменной
        int count = 0; 
        byte[] bt = null; // буфер приемный
        ProcessStartInfo load_ecg = new ProcessStartInfo(); // запус программы ecg

        public Form1()
        {
            InitializeComponent();
            string[] ports = SerialPort.GetPortNames();
            comboBox1.Items.AddRange(ports);
            
        }
        
        //настройки порта 
        //и чтение данных
        private void button1_Click(object sender, EventArgs e)
        {
          try
            {
                Program.port.PortName = comboBox1.Text;
                Program.port.BaudRate = Convert.ToInt32(comboBox2.Text);
                Program.port.StopBits = StopBits.One;
                Program.port.Parity = Parity.None;
                Program.port.DataBits = 8;
                Program.port.Open();
                if (!Program.port.IsOpen) { return; }
               
                while (true)
                {
                    ecgSizeFromSP = ReadData();

                    if (ecgSizeFromSP != 0) break;
                }
                if (ecgSizeFromSP != 0)
                {
                    Program.port.DataReceived += new SerialDataReceivedEventHandler(DataSerial);
                }
                else return;
            }
            catch (Exception ex) { MessageBox.Show("Ошибка: " + ex.Message); Program.port.Close(); return; }
        }
        
        //читает заголовок с порта
        //парсит полученные данные
        public int ReadData()
        {
            int ecgSize = 0;
            try
            {
                while (true)
                {
                    string str = Program.port.ReadExisting();

                    if (str != "")
                    {
                        string[] buff = str.Split('#');
                        ecgFileName = buff[1];
                        int.TryParse(buff[2], out ecgSize);
                        f_use = File.Open(@"..\" + ecgFileName, FileMode.OpenOrCreate);
                        return ecgSize;
                    }
                    else return 0;
                }
            }
            catch (Exception ex) { MessageBox.Show("Error"+ex.Message); return 0; }
        }


        //Чтение кардиограммы
        // запись кардиограммы в файл
        private void DataSerial(object sender, SerialDataReceivedEventArgs e)
        {
           try
            {
                Thread.Sleep(300);
                int rdata = Program.port.BytesToRead;
                Thread.Sleep(200);

                if (rdata != 0)
                {
                    mdata = new byte[rdata];
                    Program.port.Read(mdata, 0, rdata);
                    buffreader.AddRange(mdata);
                    count =count+ rdata;
                    if (count == ecgSizeFromSP)
                    {
                     this.BeginInvoke(new DelOntextbox(Ontextbox), buffreader);
                     count = 0;
                     Program.port.Close(); writer.Close();
                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }
        private delegate void DelOntextbox(List<byte> data);

        //запускает программу ecg 
        //выводит кардиограмму на экран
        private void Ontextbox(List<byte> data)
        {
            load_ecg.FileName = "D:\\Axion ECG Server\\ECG.exe";
            load_ecg.Arguments = ecgFileName;
             writer = new BinaryWriter(f_use);
            try
            {
                bt = data.ToArray();
                writer.Write(bt);
               // Process.Start("D:\\Axion ECG Server\\ECG.exe", ecgFileName);
                Process.Start(load_ecg);
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }
        private void button3_Click(object sender, EventArgs e)
        {
            comboBox1.Items.Clear();
            string[] ports = SerialPort.GetPortNames();
            comboBox1.Items.AddRange(ports);
            //удаление файла с таким же именем
            //FileInfo aFile = new FileInfo(@"..\bt_data.dat");
            //if (aFile.Exists == true) aFile.Delete();
        }
        private void button2_Click(object sender, EventArgs e)
        {
            Program.port.Close();

        }
    }
}
